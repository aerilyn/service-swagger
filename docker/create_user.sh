#!/bin/sh
# vim:sw=4:ts=4:et

USERNAME=$1
PASSWORD=$2

if [ "$USERNAME" == "" ]
then
    echo "First argument required, username will create"
    exit 1
fi

if [ "$PASSWORD" == "" ]
then
    echo "First argument required, password for this user"
    exit 1
fi

htpasswd -b /etc/nginx/.htpasswd $USERNAME $PASSWORD