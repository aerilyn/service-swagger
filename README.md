# service-swagger

## Getting started

1. clone this repo to your local
2. execute this command to up this docker
```
make serviceUp
```
3. you can access this api documentation to link
```
http://localhost:8001/api-documentation
```
4. username and passord for basic auth when access swagger
```
username: admin
password: admin123
```
5. if you want to create new username you can go to folder service-swagger then execute command below
```
make createUser USERNAME=username PASSWORD=password
```

if you need help you can contact contributor. Thanks.