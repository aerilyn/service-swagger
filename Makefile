.PHONY: serviceUp
serviceUp:
	@echo "===== RUNNING SERVICES ====="
	docker-compose up -d --build || exit 1

.PHONY: serviceDown
serviceDown:
	@echo "===== DOWN SERVICES ====="
	docker-compose down || exit 1

.PHONY: createUser
createUser:
	docker exec -it swagger-ui ./etc/nginx/create_user.sh $(USERNAME)  $(PASSWORD) || exit 1